from django.conf.urls import url
import views

urlpatterns = [
    url(r'^users/$', views.siteuser_list),
    url(r'^users/(?P<pk>[0-9]+)$', views.siteuser_detail),
]