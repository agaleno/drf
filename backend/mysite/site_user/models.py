# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

class SiteUser(models.Model):
    email = models.EmailField(max_length=254, blank=False)
    first_name = models.CharField(max_length=30, blank=True)
    last_name = models.CharField(max_length=30, blank=True)
    password = models.CharField(max_length=254, blank=True)
    is_active = models.BooleanField(default=False)

    class Meta:
        ordering = ('id',)