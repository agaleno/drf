# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import JsonResponse, HttpResponse
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from models import SiteUser
from serializer import SiteUserSerializer


def siteuser_list(request):
    """
    List all site users
    """

    if request.method == 'GET':
        users = SiteUser.objects.all()
        serializer = SiteUserSerializer(users, many=True)
        return JsonResponse(serializer.data, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = SiteUser(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)


def siteuser_detail(request, pk):
    """
    Retrieve, update or delete a code snippet.
    """
    try:
        user = SiteUser.objects.get(pk=pk)
    except SiteUser.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = SiteUserSerializer(user)
        return JsonResponse(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = SiteUserSerializer(user, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        user.delete()
        return HttpResponse(status=204)